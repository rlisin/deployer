# The tiny bitbucket to virtual host deployer #

This is an alternative solution to deploy a git project from bitbucket *on push* to a virtualhost that don't have installed **git**/**capistrano**/etc.

### How to use? ###

* [Generate a deployment key](https://confluence.atlassian.com/x/I4CNEQ) protected with a password.
* Rename your key to something like 
```
.htdeployer
```

* Upload the ```deployer.php``` from the [archive](https://bitbucket.org/rlisin/deployer/get/master.zip) with your key file ```.htdeployer``` to your PHP hosting.
* Create a git-push [hook](https://confluence.atlassian.com/display/BITBUCKET/Manage+Webhooks) with next URL parameters:

1. *username* - target bitbucket repository owner
2. *repository* - repository name
3. *password* - password of the deployment key

Example hook URL should look like this:
```
http://www.lisin.ru/deployer.php?username=rlisin&repository=deployer&password=123
```