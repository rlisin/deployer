<?php
$username   = "rlisin";
$reponame   = "deployer";
$key        = '.htdeployer'; // Deployment key
$keypass    = isset($_REQUEST['password']) ? $_REQUEST['password'] : "";

$isRedeploy = isset($_REQUEST["redeploy"]);
$dest       = "./"; // leave ./ for relative destination
$zname = "deployer.zip";

////////////////////////////////////////////////////////

set_time_limit(1000);
ini_set("display_errors", 1);
deployerDownloadNode("https://bitbucket.org/$username/$reponame/get/master.zip", $zname, $key, $keypass, $dest);
die;

if ($isRedeploy) {
	copy_recursively("$username-$reponame-$node", $dest);
} elseif (!isset($_POST['payload'])) {
	die("A POST request is required");
} else {
	$json = stripslashes($_POST['payload']);
	$data = json_decode($json);
	$node = $data->commits[0]->node;
	echo $json;
	die;
	deployerApplyPull($files, $username, $reponame, $node);
}

function deployerDownloadNode($url, $zipName, $key, $keypass, $dest) {
	$fp = fopen($zipName, 'wb');
	$ch = curl_init($url);
	curl_setopt_array($ch, array(
      CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_SSLKEY => $key,
	  CURLOPT_SSLKEYPASSWD => $keypass,
	  CURLOPT_FILE => $fp,
	  CURLOPT_VERBOSE => TRUE,
	  CURLOPT_CAINFO => 'C:\usr\ca-bundle\fb_ca_chain_bundle.crt'
	  )
	);
	curl_exec($ch);
	if (curl_errno($ch)) {
		echo curl_error($ch);
	}
	curl_close($ch);
	fclose($fp);
	
	chmod($zipName, 0600);

	$zip = new ZipArchive;
	$res = $zip->open($zipName);
	if ($res === TRUE) {
		$zip->extractTo($dest);
		$zip->close();
	} else {
		die('ZIP not supported on this server!');
	}
}

function deployerApplyPull($files, $username, $reponame, $node) {
	foreach ($files as $file) {
		if ($file->type == "removed") {
			echo "Deleting ".$file->file, PHP_EOL;
			@unlink($file->file);
		} else {
			echo "Adding/replacing ".$file->file, PHP_EOL;
			@copy("$username-$reponame-$node".$file->file, $file->file);
		}
	}
}


// function to delete all files in a directory recursively
function rmdir_recursively($dir) { 
    if (is_dir($dir)) { 
        $objects = scandir($dir); 
        foreach ($objects as $object) { 
            if ($object != "." && $object != "..") { 
                if (filetype($dir."/".$object) == "dir") rmdir_recursively($dir."/".$object); else unlink($dir."/".$object); 
            } 
        } 
        reset($objects); 
        rmdir($dir); 
    } 
} 

// function to recursively copy the files
function copy_recursively($src, $dest) {
    if (is_dir($src)) {
		if($dest != "./") rmdir_recursively($dest);
		@mkdir($dest);
		$files = scandir($src);
		foreach ($files as $file)
			if ($file != "." && $file != "..") copy_recursively("$src/$file", "$dest/$file"); 
		}
    else if (file_exists($src)) copy($src, $dest);
    rmdir_recursively($src);
}

unlink($zname);
rmdir_recursively("$username-$reponame-$node");
echo "We're done!";

?>